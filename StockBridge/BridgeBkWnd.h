#pragma once
#include "BridgeWnd.h"


class BridgeBkWnd:public WindowImplBase
{
public:
	BridgeBkWnd();
	~BridgeBkWnd();

	virtual void OnFinalMessage(HWND hWnd);
	virtual CDuiString GetSkinFile() { return _T("background.xml"); };
	virtual LPCTSTR GetWindowClassName(void) const { return _T("backgroup_dlg"); };
	virtual void InitWindow();

	
	LRESULT HandleCustomMessage(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled);

	void SetWndParent(HWND hWnd) { m_hWndParent = hWnd; };
	
	//显示或隐藏窗口，用于Mdi切换子窗口
	void SetWndIsVisiable(bool isShow) { m_bIsWndVisiable = isShow; };

	void OnInitlizeWndSize();

	void IsWndInParentWnd(WINDOWPOS* pWndPos);
	
	//外部调用是否显示窗口,用于Mdi客户端的显示使用
	void SetLastWndStatus(bool isStockWnd, bool isSetFoucs);

	void ShowStockBasic(void* pStock);
private:
	friend class BridgeWnd;
	BridgeWnd* wndBasicInfo;
	HWND m_hWndParent;
	bool m_bIsWndVisiable;
};

