#include "pch.h"
#include "comdef.h"
#include "BridgeBkWnd.h"
#include "BridgeWnd.h"
#include "Common/comdef.h"
#include "Common/Utils.h"
#include "ThirdParty\httlib\httplib.h"
#include "ThirdParty\json\json.hpp"


DUI_BEGIN_MESSAGE_MAP(BridgeWnd, WindowImplBase)
DUI_ON_MSGTYPE(DUI_MSGTYPE_CLICK, OnClick)
DUI_END_MESSAGE_MAP()


BridgeWnd::BridgeWnd(WindowImplBase* hParent)
	:m_hParent((BridgeBkWnd*)hParent)
	,m_wndDraggable(true)
{

}

BridgeWnd::~BridgeWnd()
{
}

void BridgeWnd::InitWindow()
{
	
	m_frmMain = this->m_pm.FindControl(_T("mainfrm"));
	m_frmToolbar = this->m_pm.FindControl(_T("toolbar"));

	//label UI
	//{xml->label名称，默认显示，与数据对称的中文名称}
	std::list<std::tuple<TCHAR*, TCHAR*, char*>> labelName = {
		{_T("industryinfo"),		_T(""),				"行业"},
		{_T("areainfo"),			_T(""),				"地域"},
		{_T("blocksinfo"),			_T("核心概念："),	"核心概念"},
		{_T("blocksl1info"),		_T("主题概念："),	"主题概念"},
		{_T("mainproductlabel"),	_T("主营业务："),	"主营业务"},
		{_T("driverelementlabel"),	_T("重要事项："),	"重要事项"},
		{_T("limituplabel"),		_T("最近涨停："),	"最近涨停"}
	};
	//_T("areainfo") ,_T("blocksl1info"),_T("blocksinfo") ,_T("mainproductlabel"),_T("driverelementlabel"),_T("limituplabel")
	for (auto& iter : labelName)
	{
		auto pCtrl = static_cast<CLabelUI*>(this->m_pm.FindControl(std::get<0>(iter)));
		if (pCtrl)
			m_labelUi[std::get<2>(iter)] = std::make_tuple(pCtrl, std::get<1>(iter));
	}


	
	OnReSetLabelUI();
	OnSetFrameSize();
	OnSetWndActive(false);
}

LRESULT BridgeWnd::HandleCustomMessage(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
	bHandled = FALSE;

	if (uMsg == WM_SETFOCUS || uMsg == WM_KILLFOCUS)
	{
		OnSetWndActive(WM_SETFOCUS == uMsg);
	}
	else if (uMsg == WM_WINDOWPOSCHANGING)
	{
		m_hParent->IsWndInParentWnd((WINDOWPOS*)lParam);
	}
	else if (uMsg == WM_SETBASICINFO)
	{
		OnSetBasicInfo();
	}

	return 0;
}

void BridgeWnd::OnClick(TNotifyUI& msg)
{
	auto senderName = msg.pSender->GetName();
	
	if (senderName.Compare(_T("closebtn")) == 0)
	{
		
		m_hParent->ShowWindow(false);
		this->ShowWindow(false);
		m_hParent->m_bIsWndVisiable = false;
		
	}
	else if (senderName.Compare(_T("lockbtn")) == 0)
	{
		auto SetWndDraggable = [this,&msg](bool isDrag)->void
		{
			RECT rect = { 0 };

			rect.bottom = isDrag ? -1 : 0;
			m_hParent->m_pm.SetCaptionRect(rect);
			auto* btnLock = static_cast<CButtonUI*>(msg.pSender);
			btnLock->SetAttribute(_T("style"), isDrag ? _T("unlock_btn_style") : _T("lock_btn_style"));
			btnLock->SetToolTip(isDrag ? _T("锁定") : _T("解锁"));
		};

		SetWndDraggable(m_wndDraggable = !m_wndDraggable);
	}
	else if (senderName.Compare(_T("systembtn")) == 0)
	{
		//setting
		//OnPraseByStockCode("002762");
	}
}

void BridgeWnd::OnSetFrameSize()
{
	RECT rc;
	RECT rcCaption = { 0 };
	::GetWindowRect(this->GetHWND(), &rc);
	
	//set layer
	m_pm.SetOpacity(0, /*0x00010101*/RGB(01, 01, 01));
	const int nAlpha = 255 * 50 / 100;
	m_hParent->m_pm.SetOpacity(nAlpha);

	
	//set caption rect
	this->m_pm.SetCaptionRect(rcCaption);
	rcCaption.bottom = rc.bottom - rc.top;
	m_hParent->m_pm.SetCaptionRect(rcCaption);
	
	
	//set position and size
	/*int cx = rc.right - rc.left;
	int cy = rc.bottom - rc.top;
	SetWindowPos((HWND)*m_hParent,FALSE, rc.left, rc.top, cx, cy, SWP_NOZORDER | SWP_NOACTIVATE);*/
}

void BridgeWnd::OnSetWndActive(bool isActive)
{
	//m_frmMain->SetBorderSize(isActive);
	m_frmMain->SetBorderColor(isActive ? 0xFF7F7F7F : 0xFF333333);
	m_frmToolbar->SetVisible(isActive);
}

void BridgeWnd::OnReSetLabelUI()
{
	for (auto& iter : m_labelUi)
	{
		auto& tup = iter.second;
		std::get<0>(tup)->SetText(std::get<1>(tup));
	}

}

void BridgeWnd::OnPraseByStockCode(const char* stockCode)
{
	if (m_stockCode != stockCode)
	{
		m_stockCode = stockCode;
		OnReSetLabelUI();
		m_uiBasic.clear();
		std::thread(&BridgeWnd::ParseRoutine, this).detach();
	}
	
}

void BridgeWnd::OnSetBasicInfo()
{
#if 0	//test
#include "ThirdParty/json/json.hpp"
#include <fstream>
#include "Common/Utils.h"
#include "ThirdParty/httlib/httplib.h"

	CDuiString jsonPath = CPaintManagerUI::GetInstancePath();
	std::ifstream ifs_lv1((jsonPath + _T("basic_l1.json")).GetData());
	std::ifstream ifs_lv2((jsonPath + _T("basic_l2.json")).GetData());
	if (!ifs_lv2.is_open())
		return;

	std::string json_text_l2((std::istreambuf_iterator<char>(ifs_lv2)), std::istreambuf_iterator<char>());
	ifs_lv2.close();

	if (!ifs_lv1.is_open())
		return;

	std::string json_text_l1((std::istreambuf_iterator<char>(ifs_lv1)), std::istreambuf_iterator<char>());
	ifs_lv1.close();
	
	
	
#pragma region lambda_utils
	auto lambda_set_text = [this](CLabelUI* label, std::string& value, std::string& tips = std::string("")) -> void
	{
		
		label->SetText(value.data());
		if (!tips.empty())
			label->SetToolTip(tips.data());
	};

	auto lamba_html_text = [](const char* title, const std::string& text, 
		uint32_t color_text = 0xAC7A00,uint32_t color_title = 0xAAAAAA)-> std::string
	{
		std::string value = text;
		CDuiString fmt;
		fmt.Format("<c #%x>%s</c><c #%x>%s</c>", color_title, title, color_text, value.data());
		return fmt.GetData();

	};

	auto lambda_block_parse = [](std::string& value)-> std::string
	{
		CDuiString strvalue(value.data());
		strvalue.Replace("$", "<c #FF3300>");
		strvalue.Replace("&", "<c #8F8F8F>");
		strvalue.Replace("!", "</c>");
		return strvalue.GetData();
	};
#pragma endregion lambda_utils

	try
	{
		using najson = nlohmann::json;
		najson doc_lv1 = najson::parse(json_text_l1);
		najson doc_lv2 = najson::parse(json_text_l2);
		
		doc_lv1 = doc_lv1["data"].get<najson>();
		doc_lv2 = doc_lv2["data"].get<najson>();
		
		
		//Set Ui Information
		std::string txtvalue;
		std::string tips;
		txtvalue = utils::utf8_to_ansi(doc_lv2["industry"]["info"].get<std::string>());
		lambda_set_text(m_industryinfo, txtvalue, txtvalue);

		txtvalue = utils::utf8_to_ansi(doc_lv2["area"]["info"].get<std::string>());
		lambda_set_text(m_areainfo, txtvalue, txtvalue);

		txtvalue = utils::utf8_to_ansi(doc_lv2["blocks"]["info"].get<std::string>());
		lambda_set_text(m_blocksinfo, lamba_html_text("核心概念：", lambda_block_parse(txtvalue), 0xAAAAAA), utils::string_remove_chars(txtvalue, "$&!"));

		txtvalue = utils::utf8_to_ansi(doc_lv1["blocks"]["info"].get<std::string>());
		lambda_set_text(m_blocksl1info, lamba_html_text("主题概念：",txtvalue), txtvalue);

		txtvalue = utils::utf8_to_ansi(doc_lv2["mainproduct"]["info"].get<std::string>());
		if(txtvalue.find("...") != std::string::npos)
			txtvalue = utils::utf8_to_ansi(doc_lv1["mainproduct"]["info"].get<std::string>());
		lambda_set_text(m_mainproductlabel, lamba_html_text("主营业务：",txtvalue), txtvalue);

		txtvalue = utils::utf8_to_ansi(doc_lv2["limitup"]["info"].get<std::string>());
		lambda_set_text(m_limituplabel, lamba_html_text("最近涨停：", txtvalue,0xFF7F50), txtvalue);

		//parse driver elements
		auto drvlist = doc_lv2["driverelement"]["info"];
		txtvalue = "";
		size_t list_size = drvlist.size();
		for (size_t i = 0 ; i < list_size ; i++)
		{
			auto& iter = drvlist[i];
			std::string tempv = utils::utf8_to_ansi(iter["label"].get<std::string>()) ;
			txtvalue += tempv;
			if (i != (list_size - 1))
				txtvalue += " | ";
			tips += (tempv + "：" + utils::utf8_to_ansi(iter["tip"].get<std::string>()) + "\r\n\r\n");
			
		}
		//txtvalue.erase(txtvalue.end() - 3, txtvalue.end());
		lambda_set_text(m_driverelementlabel, lamba_html_text("重要事项：",txtvalue , 0xFF7F50), tips);
		

	}
	catch (std::exception& e) {
		DUITRACE(_T("%s"), e.what());
	}

#endif

	if (m_uiBasic.empty())
		return;

	
	for (auto& iter: m_labelUi)
	{
		auto labelUi = std::get<0>(iter.second);
		auto& basic_tup = m_uiBasic[iter.first];
		labelUi->SetText(std::get<0>(basic_tup).data());
		labelUi->SetToolTip(std::get<1>(basic_tup).data());
	}
}

int  BridgeWnd::ParseRoutine()
{
	std::lock_guard<mutex> guard(mtx);
	if (this->m_stockCode.empty())
		return 1;

	std::string body_l1;
	std::string body_l2;

	{
		auto lamba_get = [&](const char* host, const char* path, std::string& body)->bool
		{
			httplib::Client cli(host);
			cli.set_connection_timeout(2, 0);

			if (auto respon = cli.Get(path))
			{
				if (respon->status == 200)
				{
					body = respon->body;
					return true;
				}
			}

			return false;
		};
		//lv1
		std::string get_path = "/api/stockph/simpleF10Data.php?code=" + this->m_stockCode;
		if (!lamba_get("basic.10jqka.com.cn", get_path.data(), body_l1))
			return 1;
		//lv2
		get_path = "/supertape/newbasicinfo/index?code=" + this->m_stockCode;
		if (!lamba_get("gpc.10jqka.com.cn", get_path.data(), body_l2))
			return 1;
	}

	try
	{
		using najson = nlohmann::json;
#pragma region lambda_utils
		auto lamba_html_text = [](const char* title, const std::string& text,
			uint32_t color_text = 0xAC7A00, uint32_t color_title = 0xAAAAAA)-> std::string
		{
			std::string value = utils::string_format("<c #%x>%s</c><c #%x>%s</c>", color_title, title, color_text, text.data());
			return value;
		};

		auto lambda_block_parse = [](const std::string& value)-> std::string
		{
			std::string replaced = value;
			replaced = utils::replace_all(replaced, "$", "<c #FF3300>");
			replaced = utils::replace_all(replaced, "&", "<c #8F8F8F>");
			replaced = utils::replace_all(replaced, "!", "</c>");
			return replaced;
		};
#pragma endregion lambda_utils
		
		najson doc_lv1 = najson::parse(body_l1);
		najson doc_lv2 = najson::parse(body_l2);

		if (doc_lv1["status_code"].get<int>() != 0)
			return 1;

		doc_lv1 = doc_lv1["data"].get<najson>();
		doc_lv2 = doc_lv2["data"].get<najson>();



		//Set Ui Information
		std::string txtvalue;
		std::string tips;
		txtvalue = utils::utf8_to_ansi(doc_lv2["industry"]["info"].get<std::string>());
		this->m_uiBasic["行业"] = std::make_tuple(txtvalue, txtvalue);


		txtvalue = utils::utf8_to_ansi(doc_lv2["area"]["info"].get<std::string>());
		this->m_uiBasic["地域"] = std::make_tuple(txtvalue, txtvalue);

		txtvalue = utils::utf8_to_ansi(doc_lv2["blocks"]["info"].get<std::string>());
		this->m_uiBasic["核心概念"] = std::make_tuple(lamba_html_text("核心概念：", lambda_block_parse(txtvalue), 0xAAAAAA), utils::string_remove_chars(txtvalue, "$&!"));


		txtvalue = utils::utf8_to_ansi(doc_lv1["blocks"]["info"].get<std::string>());
		this->m_uiBasic["主题概念"] = std::make_tuple(lamba_html_text("主题概念：", txtvalue), txtvalue);

		txtvalue = utils::utf8_to_ansi(doc_lv2["mainproduct"]["info"].get<std::string>());
		if (txtvalue.find("...") != std::string::npos)
			txtvalue = utils::utf8_to_ansi(doc_lv1["mainproduct"]["info"].get<std::string>());
		this->m_uiBasic["主营业务"] = std::make_tuple(lamba_html_text("主营业务：", txtvalue), txtvalue);

		txtvalue = utils::utf8_to_ansi(doc_lv2["limitup"]["info"].get<std::string>());
		this->m_uiBasic["最近涨停"] = std::make_tuple(lamba_html_text("最近涨停：", txtvalue, 0xFF7F50), txtvalue);


		//parse driver elements
		auto drvlist = doc_lv2["driverelement"]["info"];
		txtvalue = "";
		size_t list_size = drvlist.size();
		for (size_t i = 0; i < list_size; i++)
		{
			auto& iter = drvlist[i];
			std::string tempv = utils::utf8_to_ansi(iter["label"].get<std::string>());
			txtvalue += tempv;
			if (i != (list_size - 1))
				txtvalue += " | ";
			tips += (tempv + "：" + utils::utf8_to_ansi(iter["tip"].get<std::string>()) + "\r\n\r\n");

		}
		this->m_uiBasic["重要事项"] = std::make_tuple(lamba_html_text("重要事项：", txtvalue, 0xFF7F50), tips);
		
		if (IsWindowVisible(this->m_hWnd))
			::PostMessageA(this->m_hWnd, WM_SETBASICINFO, 0, 0);
	}
	catch (const std::exception&)
	{
		return 1;
	}

	return 0;
}