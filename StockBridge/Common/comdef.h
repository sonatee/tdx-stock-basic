#pragma once
#include <windows.h>

#define UI_SKIN_PATH		 _T("skin\\supertape\\")

#define WM_SETBASICINFO		WM_USER + 325


//Tdx Offsests
#define OFS_MDI_STOCK_WNDPROC		0x00446EC0
#define OFS_CWND_FRMHWND			0x20
#define OFS_CWND_STOCKPTR			0x82

#define OFS_CSTOCK_NAMEPTR			0x54
#define OFS_CSTOCK_TYPE				0xAF		//DWORD
#define OFS_CSTOCK_REGION			0xB3		//WORD

#define WM_CMDI_MIN					WM_USER + 1200
#define WM_CMDI_MAX					WM_USER + 1210

enum class enuStockMainType : int
{
	//指数行业
	Index = 0,
	
	//沪深A股
	Normal = 1,

	//more
};

enum class enuStockRegion : short
{
	//深圳
	ShenZhen = 0,
	//上海
	ShangHai = 1,

	//基金
	Fund = 0x21,
};