#pragma once
#define WIN32_LEAN_AND_MEAN
#define _CRT_SECURE_NO_DEPRECATE
#define UILIB_STATIC

#include <windows.h>
#include <objbase.h>
#include <zmouse.h>

#include "ThirdParty/minhook/include/MinHook.h"
#include "DuiLib/UIlib.h"
#include "Common/Utils.h"

//自己加载调试设置为0
#define UI_ENABLE_ALL	1

#ifdef _DEBUG
#   ifdef _UNICODE
#       pragma comment(lib, "..\\bin\\lib\\DuiLib_d.lib")
#   else
#       pragma comment(lib, "..\\bin\\lib\\DuiLibA_d.lib")
#   endif
#	pragma comment(lib, "..\\bin\\lib\\libMinHook.x86_d.lib")
#else
#   ifdef _UNICODE
#       pragma comment(lib, "..\\bin\\lib\\DuiLib.lib")
#   else
#       pragma comment(lib, "..\\bin\\lib\\DuiLibA.lib")
#   endif
#	pragma comment(lib, "..\\bin\\lib\\libMinHook.x86.lib")
#endif

#if defined _M_IX86
#pragma comment(linker, "/manifestdependency:\"type='win32' name='Microsoft.Windows.Common-Controls' version='6.0.0.0' processorArchitecture='x86' publicKeyToken='6595b64144ccf1df' language='*'\"")
#elif defined _M_IA64
#pragma comment(linker, "/manifestdependency:\"type='win32' name='Microsoft.Windows.Common-Controls' version='6.0.0.0' processorArchitecture='ia64' publicKeyToken='6595b64144ccf1df' language='*'\"")
#elif defined _M_X64
#pragma comment(linker, "/manifestdependency:\"type='win32' name='Microsoft.Windows.Common-Controls' version='6.0.0.0' processorArchitecture='amd64' publicKeyToken='6595b64144ccf1df' language='*'\"")
#else
#pragma comment(linker, "/manifestdependency:\"type='win32' name='Microsoft.Windows.Common-Controls' version='6.0.0.0' processorArchitecture='*' publicKeyToken='6595b64144ccf1df' language='*'\"")
#endif

using namespace DuiLib;